# coyo-scripts

This workspace contains helper scripts and snippets for the social intranet software coyo.

Each snippet contains Comments explaining function and scope.

- [List Users and Download as CSV-Table](list-users.md)
- [List Content Types like Wiki Articles from all Pages & Workspaces](list-wikis.md)