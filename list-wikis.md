# List all Applications matching a certain type in all Workspaces

This script fetches all Workspaces and Pages and then queries the `/senders/{id}/apps` Endpoint
to get the application type. Then the /wiki/articles Endpoint is used to get all articles for
all Wikis. This script can easily be adapted to list all other content types as well, like all
polls or all blog-posts.

```javascript
// First list all Workspaces and Pages, then fetch all Apps for all these ids
let workspaces = (await (await fetch('/web/workspaces?_pageSize=100000&_orderBy=name'))
    .json()).content.map(c => c.id);
let pages = (await (await fetch('/web/pages?_pageSize=100000&_orderBy=name'))
    .json()).content.map(c => c.id);
let appTree = await Promise.all([...workspaces,...pages]
    .map(async id => (await (await fetch(`/web/senders/${id}/apps`)).json())));

// Fetch all Articles for each app of type "wiki"
let apps = appTree.flatMap(a => a.filter(app => app.key ===  'wiki'));
await Promise.all(apps.forEach(async app => app.articles = (await (await fetch(
  `/web/senders/${app.senderId}/apps/${app.id}/wiki/articles?_pageSize=10000`)).json()).content));

// Concatenate CSV-File with Leading Byte Order Mark for Unicode
let data = '\uFEFF' + 'Workspace;Wiki;#Artikel;Titel\n' + 
  apps.map(app => app.senderName + ';' + app.name + ';' + app.articles.length + ';' + 
    app.articles
    .filter(a => a.displayName !== 'Root')
    .map(a => a.displayName)
    .join(';'))
  .join('\n') + '\n';

// Download CSV as File
let blob = new Blob([data], {type: 'text/csv'});
let elem = window.document.createElement('a');
elem.href = window.URL.createObjectURL(blob);
elem.download = 'wikis.csv';
document.body.appendChild(elem);
elem.click();
document.body.removeChild(elem);
```

## Copy and Run Scripts

The following Scripts are minified and can just be copy & pasted into the browser address bar,
while you are logged in on the coyo page.

Attention: Many browsers will strip the beginning `javascript:` when you paste this code into the address bar, as a security feature. In this case you will have to add javascript: in front of the address by hand.

If you need these scripts regularly, you can create a bookmark in your browser with the address line containing the code, then you can generate the user-list with a single click on the bookmark.

    javascript:((async () => {let workspaces = (await (await fetch('/web/workspaces?_pageSize=100000&_orderBy=name')).json()).content.map(c => c.id);let pages = (await (await fetch('/web/pages?_pageSize=100000&_orderBy=name')).json()).content.map(c => c.id);let appTree = await Promise.all([...workspaces,...pages].map(async id => (await (await fetch(`/web/senders/${id}/apps`)).json())));let apps = appTree.flatMap(a => a.filter(app => app.key ===  'wiki'));await Promise.all(apps.map(async app => app.articles = (await (await fetch(`/web/senders/${app.senderId}/apps/${app.id}/wiki/articles?_pageSize=10000`)).json()).content));let data = '\uFEFF' + 'Workspace;Wiki;#Artikel;Titel\n' + apps.map(app => app.senderName + ';' + app.name + ';' + app.articles.length + ';' + app.articles.filter(a => a.displayName !== 'Root').map(a => a.displayName).join(';')).join('\n') + '\n';let blob = new Blob([data], {type: 'text/csv'});let elem = window.document.createElement('a');elem.href = window.URL.createObjectURL(blob);elem.download = 'wikis.csv';document.body.appendChild(elem);elem.click();document.body.removeChild(elem);})())