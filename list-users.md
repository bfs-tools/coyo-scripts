# List Coyo Users with filters and export as table

Coyo exposes the /users endpoint, which lists all users visible to the currently logged in user.
To list additional fields and hidden users, one should be logged in with an admin account.

Endpoint: /users?_pageSize=10000&includeInactiveUsers=true&with=adminFields

The following parameters are relevant:
- _pageSize is the maximum amount of users which will be returned in a single call,
  choose high enough for your organisation
- includeInactiveUsers if true will also display Users which have never logged in
- with=adminFields can only be used if logged in as admin and will return additional fields

## Download List as CSV-Table

The following Script will extract all fields in the provided list from the user-json and
join them to a list which software like Excel can display. This data is then presented as a download:

```javascript
let visibleFields = ['superadmin', 'id', 'externalWorkspaceMember', 'loginName', 
  'moderatorMode', 'status', 'firstname', 'lastname', 'loginCount', 'roles'];

let users = await (await fetch("/web/users?_pageSize=5000&includeInactiveUsers=true")).json();

// Data starts with the Unicode Byte-Order Character
// Then the Headlines are extracted from the Fieldnames of the first User
let data = '\uFEFF' + 
  Object.keys(users.content[0])
  .filter(k => visibleFields.includes(k))
  .map(k => '"'+k+'"')
  .join(';') + '\n' + 
  // Now Each User is written as a row, each attribute lined by "" and separated by ;
  users.content.map(c => Object.keys(c)
    .filter(k => visibleFields.includes(k))
    // The roles list is mapped into a single field in the final table
    .map(k => '"'+ ( k=='roles' ? c[k].map(r => r.displayName).join(', ') : c[k]+'' ).replace('"',"'")+'"')
    .join(';'))
  .join('\n') + '\n';

let blob = new Blob([data], {type: 'text/csv'});
let elem = window.document.createElement('a');
elem.href = window.URL.createObjectURL(blob);
elem.download = 'users.csv';
document.body.appendChild(elem);
elem.click();
document.body.removeChild(elem);
```

## Copy and Run Scripts

The following Scripts are minified and can just be copy & pasted into the browser address bar,
while you are logged in on the coyo page.

Attention: Many browsers will strip the beginning `javascript:` when you paste this code into the address bar, as a security feature. In this case you will have to add javascript: in front of the address by hand.

If you need these scripts regularly, you can create a bookmark in your browser with the address line containing the code, then you can generate the user-list with a single click on the bookmark.

Normal Users:

    javascript:((async () => {let visibleFields = ['superadmin', 'id', 'externalWorkspaceMember', 'loginName', 'moderatorMode', 'status', 'firstname', 'lastname', 'loginCount', 'roles'];let users = await (await fetch("/web/users?_pageSize=5000&includeInactiveUsers=true")).json();let data = '\uFEFF' + Object.keys(users.content[0]).filter(k => visibleFields.includes(k)).map(k => '"'+k+'"').join(';') + '\n' + users.content.map(c => Object.keys(c).filter(k => visibleFields.includes(k)).map(k => '"'+(k=='roles' ? c[k].map(r => r.displayName).join(', ') : c[k]+'').replace('"',"'")+'"').join(';')).join('\n') + '\n';let blob = new Blob([data], {type: 'text/csv'});let elem = window.document.createElement('a');elem.href = window.URL.createObjectURL(blob);elem.download = 'users.csv';document.body.appendChild(elem);elem.click();document.body.removeChild(elem);})()) 

Admins:

    javascript:((async () => {let visibleFields = ['superadmin', 'id', 'externalWorkspaceMember', 'loginName', 'moderatorMode', 'status', 'firstname', 'lastname', 'loginCount', 'roles'];let users = await (await fetch("/web/users?_pageSize=5000&includeInactiveUsers=true&with=adminFields")).json();let data = '\uFEFF' + Object.keys(users.content[0]).filter(k => visibleFields.includes(k)).map(k => '"'+k+'"').join(';') + '\n' + users.content.map(c => Object.keys(c).filter(k => visibleFields.includes(k)).map(k => '"'+(k=='roles' ? c[k].map(r => r.displayName).join(', ') : c[k]+'').replace('"',"'")+'"').join(';')).join('\n') + '\n';let blob = new Blob([data], {type: 'text/csv'});let elem = window.document.createElement('a');elem.href = window.URL.createObjectURL(blob);elem.download = 'users.csv';document.body.appendChild(elem);elem.click();document.body.removeChild(elem);})()) 